package com.example.listview;

public class KontakModel {

    private String name, noHp, img;

    public KontakModel(String nama, String nohp, String img) {
        this.name = nama;
        this.noHp = nohp;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
