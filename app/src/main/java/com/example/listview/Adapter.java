package com.example.listview;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.mhsViewHolder> {

    private ArrayList<KontakModel> datalist;

    public Adapter(ArrayList<KontakModel> datalist){
        this.datalist = datalist;
    }

    @Override
    public mhsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.kontak_list, parent, false);
        return new mhsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(mhsViewHolder holder, int position) {
        final KontakModel kontakModel = datalist.get(position);

        holder.txtNama.setText(datalist.get(position).getName());
        holder.txtNoHp.setText(datalist.get(position).getNoHp());
        Picasso.get().
                load(kontakModel.getImg()).
                placeholder(R.drawable.loading).
                error(R.drawable.ic_launcher_foreground).
                fit().
                into(holder.imgView);

        Animation animation = AnimationUtils.loadAnimation(datalist, android.R.anim.slide_in_left);

    }

    public int getItemCount () {
        return (datalist != null) ? datalist.size() : 0;
    }

    public class mhsViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtNoHp;
        private ImageView imgView;

        public mhsViewHolder(View view){
            super(view);
            txtNama = (TextView) itemView.findViewById(R.id.txt_nama_kontak);
            txtNoHp = (TextView) itemView.findViewById(R.id.txt_nohp_user);
            imgView = (ImageView) itemView.findViewById(R.id.imgView);
        }
    }


}
