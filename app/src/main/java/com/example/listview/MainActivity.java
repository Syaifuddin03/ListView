package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private  Adapter adapter;
    private ArrayList<KontakModel> kontakModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        addData();

        recyclerView = findViewById(R.id.recycle_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new Adapter(kontakModels);
        recyclerView.setAdapter(adapter);


    }

    private void addData() {
        kontakModels = new ArrayList<>();
        kontakModels.add(new KontakModel ("Sholeh", "085367406370", "https://carlisletheacarlisletheatre.org/images/person-clipart-circle-9.png"));
        kontakModels.add(new KontakModel ("Ana", "089637370534", "https://carlisletheacarlisletheatre.org/images/user-icon-avatar-9.png"));
        kontakModels.add(new KontakModel ("Tamin", "087121423034", "https://carlisletheacarlisletheatre.org/images/user-icon-avatar-5.png"));
        kontakModels.add(new KontakModel ("Anang", "083437809858", "https://carlisletheacarlisletheatre.org/images/user-icon-avatar-6.jpg"));
        kontakModels.add(new KontakModel ("Sholeh", "085367406370", "https://carlisletheacarlisletheatre.org/images/person-clipart-circle-9.png"));
        kontakModels.add(new KontakModel ("Ana", "089637370534", "https://carlisletheacarlisletheatre.org/images/user-icon-avatar-9.png"));
        kontakModels.add(new KontakModel ("Tamin", "087121423034", "https://carlisletheacarlisletheatre.org/images/user-icon-avatar-5.png"));
        kontakModels.add(new KontakModel ("Anang", "083437809858", "https://carlisletheacarlisletheatre.org/images/user-icon-avatar-6.jpg"));
        kontakModels.add(new KontakModel ("Sholeh", "085367406370", "https://carlisletheacarlisletheatre.org/images/person-clipart-circle-9.png"));
        kontakModels.add(new KontakModel ("Ana", "089637370534", "https://carlisletheacarlisletheatre.org/images/user-icon-avatar-9.png"));
        kontakModels.add(new KontakModel ("Tamin", "087121423034", "https://carlisletheacarlisletheatre.org/images/user-icon-avatar-5.png"));
        kontakModels.add(new KontakModel ("Anang", "083437809858", "https://carlisletheacarlisletheatre.org/images/user-icon-avatar-6.jpg"));
    }
}
